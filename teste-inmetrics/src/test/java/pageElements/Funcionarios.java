package pageElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import funcoes.DriverFactory;

//Repositorio de elementos web tela de funcionarios
public class Funcionarios {
	WebDriver driver = null;

	public static WebElement btn_novo_funcionario() {
		return DriverFactory.getDriver().findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[2]/a"));
	}

	public static WebElement txt_nome_funcionario() {
		return DriverFactory.getDriver().findElement(By.name("nome"));
	}

	public static WebElement txt_cargo_funcionario() {
		return DriverFactory.getDriver().findElement(By.name("cargo"));
	}

	public static WebElement txt_cpf_funcionario() {
		return DriverFactory.getDriver().findElement(By.name("cpf"));
	}

	public static WebElement txt_salario_funcionario() {
		return DriverFactory.getDriver().findElement(By.name("salario"));
	}

	public static WebElement cmb_sexo_funcionario() {
		return DriverFactory.getDriver().findElement(By.id("slctSexo"));
	}

	public static WebElement radio_contratacao_funcionario() {
		return DriverFactory.getDriver().findElement(By.xpath("//*[@id=\"clt\"]"));
	}

	public static WebElement txt_admissao_funcionario() {
		return DriverFactory.getDriver().findElement(By.name("admissao"));
	}

	public static WebElement btn_confirmar_cadastro() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div/div[2]/div/form/div[3]/input"));
	}

	public static WebElement btn_confirmar_atualizacao_cadastro() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div/div/div/form/div[3]/input"));
	}

	public static WebElement lbl_confirmacao_cadastro() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div[1]/div[1]/div"));
	}

	public static WebElement txt_filtro_funcionario() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/label/input"));
	}

	public static WebElement btn_editar_funcionario() {
		return DriverFactory.getDriver()
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/table/tbody/tr[1]/td[6]/a[2]/button/span"));
	}

	public static WebElement btn_deletar_funcionario() {
		return DriverFactory.getDriver()
				.findElement(By.xpath("/html/body/div[1]/div[2]/div/table/tbody/tr[1]/td[6]/a[1]/button/span"));
	}

}
