package pageElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import funcoes.DriverFactory;

//	Repositorio de elementos web tela Login e Cadastro
public class LoginCadastro {

	static WebDriver driver;
	
	public static WebElement user_login() {
		return DriverFactory.getDriver().findElement(By.name("username"));
	}
	public static WebElement password_login() {
		return DriverFactory.getDriver().findElement(By.name("pass"));
	}
	public static WebElement btn_logar() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div/div[2]/div/form/div[6]/button"));
	}
	public static WebElement btn_novo_cadastro() {
		return DriverFactory.getDriver().findElement(By.xpath(".//*[contains(text(),'Cadastre-se')]"));
	}
	public static WebElement password_confirm() {
		return DriverFactory.getDriver().findElement(By.name("confirmpass"));
	}
	public static WebElement btn_confirmar_cadastro() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div/div/div/form/div[7]/button"));
	}
	public static WebElement validar_login() {
		return DriverFactory.getDriver().findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[1]/a"));
	}
	public static WebElement validar_ja_cadastrado() {
		return DriverFactory.getDriver().findElement(By.xpath("/html/body/div/div/div/form/div[1]/span/div"));
	}
}
