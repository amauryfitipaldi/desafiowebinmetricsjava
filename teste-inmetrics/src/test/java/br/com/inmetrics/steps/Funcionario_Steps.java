package br.com.inmetrics.steps;

import funcoes.DriverFactory;
import funcoes.Funcionalidades;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class Funcionario_Steps {

	@Dado("que realizei com login {string} e senha {string} com sucesso")
	public void que_realizei_com_login_e_senha_com_sucesso(String usuario, String senha) {
		DriverFactory.getDriver().get("http://www.inmrobo.tk/accounts/login/");
		Funcionalidades.realizar_login(usuario, senha);
		Funcionalidades.validar_login_funcionarios();
	}
	
	@Quando("incluir um novo funcionario preenchendo os campos de cadastro com dados validos")
	public void incluir_um_novo_funcionario_preenchendo_os_campos_de_cadastro_com_dados_validos() {
		Funcionalidades.inserir_novo_funcionario();
	}

	@Entao("verifico usuario cadastro com sucesso")
	public void VerificoUsuarioCadastroComSucesso() {
		Funcionalidades.validar_novo_funcionario_cadastrado();
	}

	@Quando("localizo um funcionario cadastrado")
	public void LocalizoUmFuncionarioCadastrado() {
		Funcionalidades.localizar_funcionario_ja_cadastrado();
	}

	@Entao("altero data de admissao enviando a solicitacao")
	public void AlteroDataDeAdmissaoEnviandoASolicitacao() {
		Funcionalidades.editar_funcionario();
	}

	@Entao("verifico a mensagem informacao atualizadas com sucesso")
	public void VerificoAMensagemInformacaoAtualizadaComSucesso() {
		Funcionalidades.validar_atualizacao_funcionario();
	}

	@Entao("Excluo o funcionario verificando a mensagem de funcionario removido com sucesso")
	public void ExcluoOFuncionarioVerificandoAMensagemDeFuncionarioRemovidoComSucesso() {
		Funcionalidades.deletar_funcionario();
	}

}
