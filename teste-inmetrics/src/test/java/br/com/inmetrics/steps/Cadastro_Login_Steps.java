package br.com.inmetrics.steps;

import funcoes.DriverFactory;
import funcoes.Funcionalidades;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class Cadastro_Login_Steps {

	@Dado("que a pagina web foi carregada com sucesso")
	public void QueAPaginaWebFoiCarregadaComSucesso() {
		DriverFactory.getDriver().get("http://www.inmrobo.tk/accounts/login/");
		Funcionalidades.validar_pagina_inicial();
	}

	@Quando("fizer um novo cadastro preenchendo o {string} ja cadastrado e {string} valido")
	public void fizer_um_novo_cadastro_preenchendo_o_ja_cadastrado_e_valido(String usuario, String senha) {
		
		Funcionalidades.realizar_novo_cadastro(usuario, senha);

	}

	@Entao("a tela retorna para o login")
	public void a_tela_retorna_para_o_login() {
		Funcionalidades.validar_novo_usuario_cadastrado_com_sucesso();
	}

	@Quando("preencher o {string} e {string} dentro do padrao")
	public void preencher_o_e_dentro_do_padrao(String usuario, String senha) {
		
		Funcionalidades.realizar_novo_cadastro(usuario, senha);
	}

	@Entao("localizo mensagem de usuario ja cadastrado")
	public void localizo_mensagem_de_usuario_ja_cadastrado() {
		Funcionalidades.validar_ja_cadastrado();
	}

	@Quando("fizer um login usando o {string} e {string} valido")
	public void fizer_um_login_usando_o_e_valido(String usuario, String senha) {
		Funcionalidades.realizar_login(usuario, senha);
	}

	@Entao("avanca para tela empregados")
	public void AvancaParaTelaEmpregados() {
		Funcionalidades.validar_login_funcionarios();
	}
}
