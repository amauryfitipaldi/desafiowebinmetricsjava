package br.com.inmetrics.steps;

import funcoes.API;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
public class API_Steps {

	
	@Dado("que executei o POST na api {string}")
	public void que_executei_o_POST_na_api(String string) {
		API.post(string);
		
	}
	
	@Entao("o codigo {string} sera retornado")
	public void o_codigo_sera_retornado(String string) {
	 
	    API.codigoPost(string);
	}
	
	
	@Entao("me retorna a lista do usuario requerido")
	public void me_retorna_a_lista_do_usuario_requerido() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}


	@Entao("me retornar a lista com todos usuarios")
	public void me_retornar_a_lista_com_todos_usuarios() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}


	@Entao("me retorna atualização inserida com sucesso")
	public void me_retorna_atualização_inserida_com_sucesso() {
	}
	
	@Dado("que executei o get para listar todos usuarios na api {string}")
	public void que_executei_o_get_para_listar_todos_usuarios_na_api(String string) {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Dado("que executei o get de usuario unico na api {string}")
	public void que_executei_o_get_de_usuario_unico_na_api(String string) {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Dado("que executei o put na api {string}")
	public void que_executei_o_put_na_api(String string) {
	    // Write code here that turns the phrase above into concrete actions
	}
	

}
