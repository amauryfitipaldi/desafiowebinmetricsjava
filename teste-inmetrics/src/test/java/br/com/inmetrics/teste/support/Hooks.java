package br.com.inmetrics.teste.support;

import funcoes.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

//	Reponsavel por armazenar os sets e gets, iniciar as classes before e after 
public class Hooks {

	private static String nome_funcionario;
	

	public void setNomeFuncionario(String sett_funcionario) {
		nome_funcionario = sett_funcionario;
	}

	public String getNomeFuncionario() {
		return nome_funcionario;
	}

	@Before
	public void inicialize(Scenario scenario) {

//		System.setProperty("webdriver.chrome.driver", "C:\\Projetos\\Java\\ChromeDriver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get("http://www.inmrobo.tk/accounts/login/");
	}

	@After
	public void finish() {
		DriverFactory.killDriver();
	}

	
}