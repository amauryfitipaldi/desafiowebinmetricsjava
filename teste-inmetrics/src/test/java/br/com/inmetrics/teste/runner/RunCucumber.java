package br.com.inmetrics.teste.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(	
		
	features = "src/test/resources/feature",
	glue = {"br.com.inmetrics.steps", "br.com.inmetrics.teste.support"},
	tags = "@21000",
	plugin = {"pretty","html:target/reports/report-html", "json:target/reports/Json/report-json"},
	monochrome = false
	
)

public class RunCucumber{
	
}
