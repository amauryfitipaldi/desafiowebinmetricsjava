package funcoes;

import com.github.javafaker.Faker;

//	Gerador Fake usado para popular diversos campos, devido ao tempo utilizei um gerador free disponibilizado na internet
//  https://dius.github.io/java-faker/

public class GeradorFake {
//Instancia para todas as funcoes
	Faker faker = new Faker();

	public String fake_nome() {
		String Nome = faker.name().fullName();
		return Nome;
	}

	public String fake_cargo() {
		String cargo = faker.options().option("Gerente", "Tester", "Recepcionista");
		return cargo;
	}

	public String fake_salario() {
		int gerador_salario = faker.number().numberBetween(1000, 3500000);
		String salario = Integer.toString(gerador_salario);
		//
		return salario;
	}

	public String fake_cpf() {

//		ao inserir CPFs a pagina não aceita mesmo sendo cpfs validos
		String cpf = faker.options().option("93871654000", "75181606091", "32097041043", "48220451025");
		return cpf;
	}

	public String fake_gender() {
		String sexo = faker.options().option("m", "f", "i");
		return sexo;
	}

	public String fake_data_admissao() {
		String data = faker.options().option("12202020", "01051991" ,"10101990");
		return data;
	}

	public String fake_tipo_contratacao() {
		String sexo = faker.options().option("clt", "pj");
		return sexo;
	}

}
