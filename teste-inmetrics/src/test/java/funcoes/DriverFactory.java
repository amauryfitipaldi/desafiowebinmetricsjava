package funcoes;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import br.com.inmetrics.teste.support.Properties;

public class DriverFactory {

	private static WebDriver driver;

	public DriverFactory() {
	}

	public void setWebDriver(Object object) {
		DriverFactory.driver = (WebDriver) object;
	}

	public static WebDriver getDriver() {

		if (driver == null) {
			switch (Properties.browser) {
			case CHROME:
				System.setProperty("webdriver.chrome.driver", "C:\\Projetos\\Java\\ChromeDriver\\chromedriver.exe");
				ChromeOptions optionsChrome = new ChromeOptions();
				// options.addArguments("--headless");
				optionsChrome.addArguments("--use-fake-ui-for-media-stream=true");
				optionsChrome.addArguments("--enable-media-stream");
				optionsChrome.addArguments("--enable-peer-connection");
				optionsChrome.addArguments("--disable-web-security chrome://version/");
				optionsChrome.addArguments("--start-maximized");
				optionsChrome.addArguments("--disable-extensions");
				optionsChrome.addArguments("--disable-javascript");
				optionsChrome.addArguments("--ignore-certificate-errors");
				optionsChrome.addArguments("--test-type");
				driver = new ChromeDriver(optionsChrome);
				break;

			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		}
		return driver;
	}

	public static void killDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}