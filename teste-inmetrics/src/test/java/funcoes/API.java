package funcoes;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import io.restassured.http.ContentType;

public class API {

	private static String codePost;

	public static void post(String api) {
		try {
			HashMap<String, String> postContent = new HashMap<>();
			postContent.put("admissao", "15122020");
			postContent.put("cargo", "tester");
			postContent.put("comissao", "10");
			postContent.put("cpf", "37385278850");
			postContent.put("departamentoId", "0");
			postContent.put("nome", "Amaury Moraes");
			postContent.put("salario", "35000");
			postContent.put("sexo", "Masculino");
			postContent.put("tipoContratacao", "clt");

			given().contentType(ContentType.JSON).body(postContent).post(api).then().statusCode(201)
					.body("admissao", equalTo("15122020")).body("cargo", equalTo("tester"))
					.body("comissao", equalTo("10")).body("cpf", equalTo("37385278850"))
					.body("departamentoId", equalTo("0")).body("salario", equalTo("35000"))
					.body("sexo", equalTo("Masculino")).body("tipoContratacao", equalTo("clt"))
					.body("nome", equalTo("Amaury Moraes")).extract().response();
			codePost = "200";
			assertTrue("Post efetuado com sucesso", true);
		} catch (Exception e) {

			assertTrue("Falha ao gerar codigo", false);
			e.getMessage();
		}
	}

	public static void codigoPost(String post) {
		try {
			if (codePost.equals(post)) {
				assertTrue("Codigo gerado com sucesso: " + codePost, true);
			}
		} catch (Exception e) {
			assertTrue("Falha ao gerar codigo", false);
		}

	}
}
