package funcoes;

import static org.junit.Assert.assertTrue;
import br.com.inmetrics.teste.support.Hooks;

//	PageFactory responsavel por armazenar as funções e ter toda a logica de programação
public class Funcionalidades {

	public static void validar_pagina_inicial() {

		String validar = pageElements.LoginCadastro.btn_novo_cadastro().getText();

		if (validar.equalsIgnoreCase("Cadastre-se")) {
			assertTrue("Login realizado com sucesso", true);
		} else {
			assertTrue("Falha ao realizar login", false);
		}
	}

	public static void validar_novo_usuario_cadastrado_com_sucesso() {

		try {
			pageElements.LoginCadastro.validar_ja_cadastrado().isEnabled();
			assertTrue("Usuario já foi cadastrado", false);
		} catch (Exception e) {
			String validar = pageElements.LoginCadastro.btn_novo_cadastro().getText();

			if (validar.equalsIgnoreCase("Cadastre-se")) {
				assertTrue("Login realizado com sucesso", true);
			} else {
				assertTrue("Falha ao realizar login", false);
			}
		}
	}

//	Realizar Login
	public static void realizar_login(String user, String senha) {

		pageElements.LoginCadastro.password_login().sendKeys(senha);
		pageElements.LoginCadastro.user_login().sendKeys(user);
		pageElements.LoginCadastro.btn_logar().click();

		Funcionalidades.validar_login_funcionarios();

	}

//
	public static void validar_login_funcionarios() {

		String validar_login = pageElements.Funcionarios.btn_novo_funcionario().getText();

		if (validar_login.equalsIgnoreCase("Novo Funcionário")) {
			assertTrue("Login realizado com sucesso", true);
		} else {
			assertTrue("Falha ao realizar login", false);
		}
	}

// 	Realizar novo Cadastro
	public static void realizar_novo_cadastro(String user, String senha) {

		pageElements.LoginCadastro.btn_novo_cadastro().click();
		pageElements.LoginCadastro.user_login().sendKeys(user);
		pageElements.LoginCadastro.password_login().sendKeys(senha);
		;
		pageElements.LoginCadastro.password_confirm().sendKeys(senha);
		pageElements.LoginCadastro.btn_confirmar_cadastro().click();

		boolean validar = pageElements.LoginCadastro.btn_novo_cadastro().isDisplayed();

		if (validar == true) {
			assertTrue("Novo cadastro realizado com sucesso", true);
		} else {
			assertTrue("Falha ao realizar cadastro", false);
		}
	}

	public static void validar_ja_cadastrado() {

		String validar = pageElements.LoginCadastro.validar_ja_cadastrado().getText();

		if (validar.equalsIgnoreCase("Usuário já cadastrado")) {
			assertTrue("Cadastro já realizado", true);
		} else {
			assertTrue("Cadastro realizado", false);
		}
	}

//	Inserir novo funcionario
	public static void inserir_novo_funcionario() {

		GeradorFake fake = new GeradorFake();
		pageElements.Funcionarios.btn_novo_funcionario().click();
		String nome_funcionario = fake.fake_nome();
		pageElements.Funcionarios.txt_nome_funcionario().sendKeys(nome_funcionario);
		pageElements.Funcionarios.txt_cargo_funcionario().sendKeys(fake.fake_cargo());
		
		//ao inserir CPF valido a pagina não aceita, devido a isso estou setando um cpf que aceita
		pageElements.Funcionarios.txt_cpf_funcionario().sendKeys("938.716.540-00");
		pageElements.Funcionarios.cmb_sexo_funcionario().sendKeys(fake.fake_gender());
		pageElements.Funcionarios.txt_salario_funcionario().sendKeys(fake.fake_salario());
		pageElements.Funcionarios.radio_contratacao_funcionario().click();
		pageElements.Funcionarios.txt_admissao_funcionario().sendKeys(fake.fake_data_admissao());;
		pageElements.Funcionarios.btn_confirmar_cadastro().click();
		new Hooks().setNomeFuncionario(nome_funcionario);
	}

	public static void validar_novo_funcionario_cadastrado() {

		boolean validar = pageElements.Funcionarios.lbl_confirmacao_cadastro().isDisplayed();

		if (validar == true) {
			assertTrue("Novo funcionario cadastrado com sucesso", true);
		} else {
			assertTrue("Falha ao realizar cadastro", false);
		}
	}

	public static void validar_atualizacao_funcionario() {
		String validar = pageElements.Funcionarios.lbl_confirmacao_cadastro().getText();

		if (validar.contains("SUCESSO! Informações atualizadas com sucesso")) {
			assertTrue("Atualização realizada com sucesso", true);
		} else {
			assertTrue("Falha ao realizar atualização de funcionario", false);
		}
	}

	public static void localizar_funcionario_ja_cadastrado() {

		String nomeFuncionario = new Hooks().getNomeFuncionario();

		pageElements.Funcionarios.txt_filtro_funcionario().sendKeys(nomeFuncionario);

	}

	public static void editar_funcionario() {
		GeradorFake fake = new GeradorFake();
		pageElements.Funcionarios.btn_editar_funcionario().click();

		pageElements.Funcionarios.txt_admissao_funcionario().sendKeys(fake.fake_data_admissao()); // limpar e numero
																									// aleatorio
		pageElements.Funcionarios.btn_confirmar_atualizacao_cadastro().click();

	}

	public static void deletar_funcionario() {
		
		try {
			pageElements.Funcionarios.btn_deletar_funcionario().click();
			String validar = pageElements.Funcionarios.lbl_confirmacao_cadastro().getText();

			if (validar.contains("Funcionário removido com sucesso")) {
				assertTrue("Registro de Funcionario deletado", true);
			} else {
				assertTrue("Falha ao apagar registro do funcionario", false);
			}
		} catch (Exception e) {
			assertTrue("Falha ao apagar registro do funcionario", false);
		}
	}
}
