# language:pt
# enconding UTF-8

@TodosCTs
Funcionalidade: Cadastro WebSite

Contexto: Abrir pagina web

Dado que a pagina web foi carregada com sucesso

@10001 
Esquema do Cenario: CT001 - Cadastrar usuario WebSite (Positivo)
Quando preencher o '<usuario>' e '<senha>' dentro do padrao
Entao a tela retorna para o login 

Exemplos:
|usuario     |senha    |
|inserir_user|Teste@123|

@10002
Esquema do Cenario: CT003 - Cadastrar usuario ja cadastrado (Negativo)
Quando fizer um novo cadastro preenchendo o '<usuario>' ja cadastrado e '<senha>' valido
Entao localizo mensagem de usuario ja cadastrado
Exemplos:
|usuario      |senha    |
|amaury.moraes|Teste@123|

@10003
Esquema do Cenario: CT004 - Realizar login (Positivo)
Quando fizer um login usando o '<usuario>' e '<senha>' valido
Entao avanca para tela empregados
Exemplos:
|usuario      |senha    |
|amaury.moraes|Teste@123|
