# language:pt
# enconding UTF-8


Funcionalidade: Testar swaggers

  @23001 
  Cenario: CT001 - Cadastrar usuário (POST) – Empregado Controller (Positivo)
  Dado que executei o POST na api "inm-api-test.herokuapp.com/empregado/cadastrar"
	Entao o codigo "200" sera retornado
		
	@23002
	Cenario: CT002 - Listar usuário cadastrados (GET) – Empregado Controller (Positivo)
	Dado que executei o get para listar todos usuarios na api "inm-api-test.herokuapp.com/empregado/list/{empregadoId}"
	Entao me retorna a lista do usuario requerido
		
@23004
	Cenario: Listar todos usuários (GET) – Empregado Controller (Positivo)
	Dado que executei o get de usuario unico na api "inm-api-test.herokuapp.com/empregado/list_all"
	Entao me retornar a lista com todos usuarios
	
	@23005
	Cenario: Alterar usuário (PUT) – Empregado Controller (Positivo)
	Dado que executei o put na api "inm-api-test.herokuapp.com/empregado/alterar/{empregadoId}"
	Entao me retorna atualização inserida com sucesso