# language:pt
# enconding UTF-8

@TodosCTs
Funcionalidade: Testar aplicacoes web

  Contexto: realizar o login no site
    Dado que realizei com login 'amaury.moraes' e senha 'Teste@123' com sucesso  

  @20001 @21000
  Cenario: CT001 - Cadastrar novo funcionario (Positivo)
    Quando incluir um novo funcionario preenchendo os campos de cadastro com dados validos
    Entao verifico usuario cadastro com sucesso

  @21000
  Cenario: CT002 - Editar funcionario (Positivo)
    Quando localizo um funcionario cadastrado
    Entao altero data de admissao enviando a solicitacao
    E verifico a mensagem informacao atualizadas com sucesso

  @21000
  Cenario: CT003 - Excluir funcionario (Positivo)
    Quando localizo um funcionario cadastrado
    Entao Excluo o funcionario verificando a mensagem de funcionario removido com sucesso
