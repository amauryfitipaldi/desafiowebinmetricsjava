## Teste Inmetrics - descrição do projeto


Desafios - Website(JAVA) - ServiçosWeb(API)


-------------- Preparação da maquina -------------- 

Pre-requisitos
Instalar JDK 13.0.02, Eclipse, maven e preparar as variaveis do ambiente

https://www.eclipse.org/downloads/

https://www.oracle.com/java/technologies/javase-jdk13-downloads.html

http://maven.apache.org/

usar gitbash ou similar para fazer o clone do projeto usando a url "https://gitlab.com/amauryfitipaldi/desafiowebinmetricsjava.git"

Após realizar os passos acima:

Abrir o Eclipse >> 
File >> Import >> From existing Maven Project >> selecionar a pasta que fez o clone do projeto > selecionar o POM correspondente > finish

Esperar o build do projeto então fazer as alterações abaixo:

Alterar o path do ChromeDriver localizado >> src/test/java/funcoes/DriverFactory.java para o path na maquina atual exemplo "C:\\Projetos\\Java\\ChromeDriver\\chromedriver.exe" >> Chrome driver localizado na pasta do projeto

Alterar JRE para versão JRE-13 --- clicar com segundo botão no projeto >> Properties >> JavaCompiler >> selecionar a versão JRE-13

-------------- Cenários de Teste --------------

Foram criados 6 CTs para WEB abrangendo o scopo do desafio,

Sendo 3 para cadastro e loginE 3 para funcionalidades de funcionarios


-------------- Execução dos CTs --------------
 
Para executar os Cenários de teste deve-se localizar o cenário desejado, copiar a @tag e inserir no runner conforme exemplo abaixo

usar atalho ctrl+shift+r ".feature", escolher qual das features deseja trabalhar.
copiar a tag desejada e inserir no "RunCucumber.java" localizado em "src/test/java/br.com.inmetrics.teste.runner"

Então executar (Segundo botão na classe "RunCucumber.Java", Run As > Junit)

Evidencia gerada na pasta Target>reports>
 

 -------------- Debito Tecnico --------------
 
   *Evidencias salvas em um report simples sendo um paliativo pela falta de um report mais robusto como allure ou extend report
   *Desafio API Não finalizado ----
   *Manter projeto no JavaSE13 ao clonar
   
   